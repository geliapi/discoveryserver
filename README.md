# Aplikasi Discovery Server #

Cara deploy ke Heroku (node US)

1. Buat aplikasi di heroku menggunakan Heroku CLI

        heroku apps:create training-discovery-us

2. Set environment variabel

        heroku config:set SPRING_PROFILES_ACTIVE=heroku,us

3. Deploy

        git push heroku master


Deploy node Eropa (region EU)

1. Buat aplikasi di Heroku

        heroku apps:create --region=eu training-discovery-eu

2. Set environment variable

        heroku config:set -a training-discovery-eu SPRING_PROFILES_ACTIVE=heroku,eu

3. Tambahkan remote git

        git remote add heroku-eu https://git.heroku.com/training-discovery-eu.git

4. Push ke server eropa

        git push heroku-eu master
